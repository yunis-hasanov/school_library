package com.scopegroup.school_library.controller;

import com.scopegroup.school_library.datatransferobject.PublicationDTO;
import com.scopegroup.school_library.exception.ConstraintsViolationException;
import com.scopegroup.school_library.model.Response;
import com.scopegroup.school_library.service.PublicationService;
import com.scopegroup.school_library.util.CSVHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("v1/publications")
public class PublicationController {

    private static final Logger logger = LoggerFactory.getLogger(PublicationController.class);

    private final PublicationService publicationService;

    public PublicationController(PublicationService publicationService) {
        this.publicationService = publicationService;
    }


    @GetMapping(value = "/")
    public ResponseEntity<Object> find(@RequestParam(value = "email", required = false) String email,
                                       @RequestParam(value = "isbn", required = false) String isbn) {
        logger.info("API received a request with the path: /");

        List<PublicationDTO> publicationDTOList = publicationService.find(email, isbn);

        return ResponseEntity.status(HttpStatus.OK)
                .body(publicationDTOList);
    }


    @PostMapping("/")
    public ResponseEntity<Object> uploadFile(@RequestParam("file") MultipartFile file) throws ConstraintsViolationException {
        if (CSVHelper.hasCSVFormat(file)) {
            Response<Object> response = publicationService.save(file);
            String msg = "All records succefully added";
            if (!response.getMessage().trim().isEmpty()) {
                msg = response.getMessage();
            }
            return ResponseEntity.status(HttpStatus.OK).body(msg);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid csv file!");
    }


}
