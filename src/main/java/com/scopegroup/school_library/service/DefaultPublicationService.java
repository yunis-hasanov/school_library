package com.scopegroup.school_library.service;

import com.scopegroup.school_library.dataaccessobject.AuthorRepository;
import com.scopegroup.school_library.dataaccessobject.PublicationRepository;
import com.scopegroup.school_library.datatransferobject.PublicationDTO;
import com.scopegroup.school_library.domainobject.AuthorDO;
import com.scopegroup.school_library.domainobject.PublicationDO;
import com.scopegroup.school_library.exception.ConstraintsViolationException;
import com.scopegroup.school_library.model.Response;
import com.scopegroup.school_library.util.CSVHelper;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DefaultPublicationService implements PublicationService {

    private final AuthorRepository authorRepository;
    private final PublicationRepository publicationRepository;

    public DefaultPublicationService(AuthorRepository authorRepository, PublicationRepository publicationRepository) {
        this.authorRepository = authorRepository;
        this.publicationRepository = publicationRepository;
    }

    @Override
    public List<PublicationDTO> find(String email, String isbn) {
        List<AuthorDO> authorList = new ArrayList<>();
        authorRepository.findAll().forEach(authorList::add);

        List<PublicationDO> publicationList = new ArrayList<>();
        publicationRepository.findAll().forEach(publicationList::add);

        if (email != null) {
            authorList = authorList.stream().filter(x -> x.getEmail().equals(email.trim())).collect(Collectors.toList());
        }

        if (isbn != null) {
            publicationList = publicationList.stream().filter(x -> x.getIsbn().equals(isbn)).collect(Collectors.toList());
        }

        List<Long> authorIds = authorList.stream().map(AuthorDO::getId).collect(Collectors.toList());

        publicationList = publicationList.stream().filter(x -> authorIds.contains(x.getAuthorId())).collect(Collectors.toList());

        List<PublicationDTO> publicationDTOs = new ArrayList<>();
        for (PublicationDO publicationDO : publicationList) {
            Optional<AuthorDO> authorDO = authorList.stream().filter(x -> x.getId() == publicationDO.getAuthorId()).findFirst();
            PublicationDTO item = new PublicationDTO();
            item.setFirstName(authorDO.get().getFirstName());
            item.setLastName(authorDO.get().getLastName());
            item.setEmail(authorDO.get().getEmail());
            item.setIsbn(publicationDO.getIsbn());
            item.setTitle(publicationDO.getTitle());
            item.setDescription(publicationDO.getDescription());
            item.setIssueDate(publicationDO.getIssueDate());
            publicationDTOs.add(item);
        }

        return publicationDTOs;
    }


    @Override
    public Response<Object> save(MultipartFile file) throws ConstraintsViolationException {
        try {
            Response<Object> response = CSVHelper.csvToPublicationDTO(file.getInputStream());
            if (response.getData() != null) {
                List<PublicationDTO> publicationDTOs = (List<PublicationDTO>) response.getData();

                for (PublicationDTO item : publicationDTOs) {
                    AuthorDO author = new AuthorDO(item.getFirstName(), item.getLastName(), item.getEmail());
                    authorRepository.save(author);
                    PublicationDO publication = new PublicationDO(author.getId(),
                            item.getPublicationType(),
                            item.getIsbn(),
                            item.getTitle(),
                            item.getDescription(),
                            item.getIssueDate());

                    publicationRepository.save(publication);
                }

            }
            return response;
        } catch (DataIntegrityViolationException e) {
            throw new ConstraintsViolationException(e.getMessage());
        } catch (IOException e) {
            return new Response<Object>("fail to store csv data: " + e.getMessage());
        }
    }


}
