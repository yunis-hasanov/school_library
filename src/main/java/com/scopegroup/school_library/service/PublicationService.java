package com.scopegroup.school_library.service;

import com.scopegroup.school_library.datatransferobject.PublicationDTO;
import com.scopegroup.school_library.exception.ConstraintsViolationException;
import com.scopegroup.school_library.model.Response;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface PublicationService {


    List<PublicationDTO> find(String email, String isbn);

    Response<Object> save(MultipartFile file) throws ConstraintsViolationException;


}
