package com.scopegroup.school_library.dataaccessobject;

import com.scopegroup.school_library.domainobject.AuthorDO;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<AuthorDO, Long> {
}
