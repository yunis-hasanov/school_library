package com.scopegroup.school_library.dataaccessobject;

import com.scopegroup.school_library.domainobject.PublicationDO;
import org.springframework.data.repository.CrudRepository;

public interface PublicationRepository extends CrudRepository<PublicationDO, Long> {
}
