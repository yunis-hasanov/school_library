package com.scopegroup.school_library.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class Response<T> {


    private String message;
    private T data;

    public Response() {
    }

    public Response(String message) {
        this.message = message;
    }

    public Response(String message, T data) {
        this.message = message;
        this.data = data;
    }


    @Override
    public String toString() {
        return "Response{" +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
