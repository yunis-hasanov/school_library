package com.scopegroup.school_library.datatransferobject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.scopegroup.school_library.domainvalue.publicationType;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Date;


@Getter
@Setter
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PublicationDTO {

    publicationType publicationType;
    @NotNull(message = "firstName can not be null!")
    private String firstName;
    @NotNull(message = "lastName can not be null!")
    private String lastName;
    @NotNull(message = "email can not be null!")
    @Email(message = "email is incorrect format!")
    private String email;
    @NotNull(message = "isbn can not be null!")
    private String isbn;

    @NotNull(message = "title can not be null!")
    private String title;

    private String description;

    private Date issueDate;


}
