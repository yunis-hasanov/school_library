package com.scopegroup.school_library.util;

import com.scopegroup.school_library.datatransferobject.PublicationDTO;
import com.scopegroup.school_library.domainvalue.publicationType;
import com.scopegroup.school_library.model.Response;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class CSVHelper {
    public static String TYPE = "text/csv";
    static String[] HEADERs = {"Id", "Title", "Description", "Published"};

    public static boolean hasCSVFormat(MultipartFile file) {

        if (!TYPE.equals(file.getContentType())) {
            return false;
        }

        return true;
    }

    public static Response<Object> csvToPublicationDTO(InputStream is) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        //email,firstName,lastName,type,isbn,title,description,issueDate
        List<PublicationDTO> publicationDTOs = new ArrayList<PublicationDTO>();
        Set<String> emails = new HashSet<>();
        StringBuffer responseMessage = new StringBuffer();
        try {
            Reader in = new InputStreamReader(is, StandardCharsets.UTF_8);
            Iterable<CSVRecord> csvRecords = CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim().parse(in);
            int recordId = 1;
            for (CSVRecord csvRecord : csvRecords) {
                PublicationDTO publication = new PublicationDTO();
                String email = csvRecord.get("email").trim();
                if (email == null || email.length() == 0) {
                    responseMessage.append("email is empty in record:" + recordId);
                    continue;
                } else {
                    if (emails.contains(email)) {
                        responseMessage.append("Email Constraints Violation in record:" + recordId);
                        continue;
                    } else {
                        emails.add(email);
                    }
                    publication.setEmail(email);
                }
                String firstName = csvRecord.get("firstName").trim();
                if (firstName == null || firstName.length() == 0) {
                    responseMessage.append("firstName is empty in record:" + recordId);
                    continue;
                } else {
                    publication.setFirstName(firstName);
                }
                String lastName = csvRecord.get("lastName").trim();
                if (lastName == null || lastName.length() == 0) {
                    responseMessage.append("lastName is empty in record:" + recordId);
                    continue;
                } else {
                    publication.setLastName(lastName);
                }
                String type = csvRecord.get("type").trim();
                if (type == null || type.length() == 0) {
                    responseMessage.append("type is empty in record:" + recordId);
                    continue;
                } else {
                    if (!type.equals(publicationType.M.name()) && !type.equals(publicationType.B.name())) {
                        responseMessage.append("unknown type in record:" + recordId);
                        continue;
                    } else {
                        if (type.equals("M")) {
                            publication.setPublicationType(publicationType.M);

                            String issueDate = csvRecord.get("issueDate").trim();
                            if (issueDate == null || issueDate.length() == 0) {
                                responseMessage.append("issueDate is empty in record:" + recordId);
                                continue;
                            } else {
                                publication.setIssueDate(formatter.parse(issueDate));
                            }
                        } else if (type.equals("B")) {
                            publication.setPublicationType(publicationType.B);
                            String description = csvRecord.get("lastName").trim();
                            if (description == null || description.length() == 0) {
                                responseMessage.append("description is empty in record:" + recordId);
                                continue;
                            } else {
                                publication.setDescription(description);
                            }
                        }

                    }
                }
                String isbn = csvRecord.get("isbn").trim();
                if (isbn == null || isbn.length() == 0) {
                    responseMessage.append("isbn is empty in record:" + recordId);
                    continue;
                } else {
                    publication.setIsbn(isbn);
                }
                String title = csvRecord.get("title").trim();
                if (title == null || title.length() == 0) {
                    responseMessage.append("title is empty in record:" + recordId);
                    continue;
                } else {
                    publication.setTitle(title);
                }

                publicationDTOs.add(publication);
                responseMessage.append("\n");
                recordId++;
            }
        } catch (Exception e) {
            return new Response<>(e.getMessage());
        }

        return new Response<>(responseMessage.toString(), publicationDTOs);

    }

}
