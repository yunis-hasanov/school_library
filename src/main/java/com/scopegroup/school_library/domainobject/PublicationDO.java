package com.scopegroup.school_library.domainobject;

import com.scopegroup.school_library.domainvalue.publicationType;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;


@Entity
@Table(
        name = "publication"
)
public class PublicationDO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "AUTHORID", nullable = false)
    @NotNull(message = "The Author ID can't be null")
    @Min(1)
    private Long authorId;

    @Column(name = "PUBLICATIONTYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private publicationType publicationType;

    @Column(name = "ISBN", nullable = false)
    @NotNull(message = "isbn can not be null!")
    private String isbn;

    @Column(name = "TITLE", nullable = false)
    @NotNull(message = "title can not be null!")
    private String title;

    @Column(name = "DESCRIPTION", nullable = true)
    private String description;

    @Column(name = "ISSUEDATE", nullable = true)
    private Date issueDate;

    public PublicationDO() {
    }

    public PublicationDO(Long authorId, publicationType publicationType, String isbn, String title, String description, Date issueDate) {
        this.authorId = authorId;
        this.publicationType = publicationType;
        this.isbn = isbn;
        this.title = title;
        this.description = description;
        this.issueDate = issueDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public com.scopegroup.school_library.domainvalue.publicationType getPublicationType() {
        return publicationType;
    }

    public void setPublicationType(com.scopegroup.school_library.domainvalue.publicationType publicationType) {
        this.publicationType = publicationType;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }
}
