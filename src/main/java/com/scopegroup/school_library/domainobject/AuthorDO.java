package com.scopegroup.school_library.domainobject;


import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Entity
@Table(
        name = "author",
        uniqueConstraints = @UniqueConstraint(name = "uc_email", columnNames = {"email"})
)
public class AuthorDO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "FIRSTNAME", nullable = false)
    @NotNull(message = "firstName can not be null!")
    private String firstName;

    @Column(name = "LASTNAME", nullable = false)
    @NotNull(message = "lastName can not be null!")
    private String lastName;

    @Column(name = "EMAIL", nullable = false)
    @NotNull(message = "email can not be null!")
    @Email(message = "email is incorrect format!")
    private String email;

    public AuthorDO() {
    }

    public AuthorDO(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
