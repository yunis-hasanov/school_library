
insert into author (id, firstName, lastName, email) values (1, 'Yunis', 'Hasanov', 'yunis.hasanov@outlook.com');

insert into publication (id, authorId, publicationType, isbn,  title, description, issueDate) values (1, 1, 'M', '123-123-456-789', 'TechCity', 'Some description blah blah...', now());