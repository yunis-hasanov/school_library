# School Library

The project is based on a small web service which uses the following technologies:

* Java 11
* Spring Boot
* Database H2 (In-Memory)
* Maven

You should be able to start the example application by executing com.scopegroup.school_library.SchoolLibraryApplication,
which starts a webserver on port 8080 (http://localhost:8080) and serves SwaggerUI where can inspect and try existing endpoints


   
 _Note: Also due to lack of time, I could'nt add a unit tests_
 
 Thank you ❤

